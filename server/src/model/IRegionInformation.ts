interface IRegionInformation {
    name: String;
    id: String;
}

export default IRegionInformation;