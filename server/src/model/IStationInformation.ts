interface IStationInformation {
    lat: number
    long: number
    regionId: string
    address: string
    name: string
    id: string
}

export default IStationInformation;