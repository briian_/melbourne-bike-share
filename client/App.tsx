import React from 'react';
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'
import BikeShareContainer from './container/BikeShareContainer';


const client = new ApolloClient({
  uri: 'HOST:4000/graphql'
});

export default function App() {
  return (
    <ApolloProvider client={client}>
      <BikeShareContainer />
    </ApolloProvider>
  );
}
